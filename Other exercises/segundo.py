#En este scrip vamos a trabajar con variables
print("Trabajando con variables")

numero=5

print("El valor de la variable numero es :" + str(numero))
print("El tipo de la variable numero es :" + str(type(numero)))

numero="5"

print("El valor de la variable numero es :" + str(numero))
print("El tipo de la variable numero es :" + str(type(numero)))

numero=5.0

print("El valor de la variable numero es :" + str(numero))
print("El tipo de la variable numero es :" + str(type(numero)))
